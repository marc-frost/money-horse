module.exports = {
  moduleNameMapper: {
    '@core/(.*)': '<rootDir>/src/app/core/$1',
    "uuid": require.resolve('uuid'),
  },
  preset: 'jest-preset-angular',
  setupFiles: ['dotenv/config'],
  setupFilesAfterEnv: ['<rootDir>/jest.polyfills.ts'],
  reporters: [
    'default',
    ['jest-junit', {outputName: 'test-report.xml'}],
  ],
  collectCoverage: true,
  collectCoverageFrom: ['src/app/**/*.ts'],
  coveragePathIgnorePatterns: [".spec.ts", ".module.ts"],
  coverageReporters: ["clover", "json", "lcov", "text", "text-summary", "cobertura"],
};
