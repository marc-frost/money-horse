import {createState, select, Store, withProps} from '@ngneat/elf';
import {localStorageStrategy, persistState} from '@ngneat/elf-persist-state';
import {Injectable} from '@angular/core';
import {map, tap} from 'rxjs';
import {TranslocoService} from '@ngneat/transloco';

export type AppSetting = {
  [name: string]: string | undefined;
}

export type App = {
  locale?: string;
  settings?: AppSetting | undefined
};

const {state, config} = createState(
  withProps<App>({}),
);

export const appStore = new Store({name: 'app', state, config});

persistState(appStore, {
  key: 'app',
  storage: localStorageStrategy,
});

@Injectable({providedIn: 'root'})
export class AppRepository {

  locale$ = appStore.pipe(
    map(x => x.locale ?? 'en'),
    tap(x => {
      this.translocoService.setActiveLang(x);
    })
  );

  settings$ = appStore.pipe(select(x => x.settings));

  constructor(private translocoService: TranslocoService) {
  }

  updateLocale(locale: string): void {
    appStore.update(x => ({...x, locale}));
  }
}
