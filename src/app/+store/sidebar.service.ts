import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export type NavItem = {
  group: string;
  label: string;
  routerLink: string[];
}

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private items = new BehaviorSubject<NavItem[]>([]);
  public items$ = this.items.asObservable();

  add(item: NavItem): SidebarService {
    this.items.next([...this.items.getValue(), item]);
    return this;
  }

  set(items: NavItem[]): SidebarService {
    this.items.next(items);
    return this;
  }
}
