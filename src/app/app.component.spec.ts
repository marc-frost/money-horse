import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslocoTestingModule} from "@ngneat/transloco";
import {AppComponent} from "./app.component";
import {RouterTestingModule} from "@angular/router/testing";
import {defaultConfig} from "./+core/test-helper";

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})
      ],
      declarations: [AppComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
