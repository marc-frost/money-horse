import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppStartComponent} from './components/app-start/app-start.component';
import {LayoutFullComponent} from './components/layout-full/layout-full.component';
import {LayoutToolsComponent} from './components/layout-tools/layout-tools.component';

const routes: Routes = [
  {
    path: 'credit',
    component: LayoutToolsComponent,
    loadChildren: () => import('./credit/credit.module').then(m => m.CreditModule),
  },
  {
    path: 'settings',
    component: LayoutToolsComponent,
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: '',
    component: LayoutFullComponent,
    children: [
      {
        path: '',
        component: AppStartComponent
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
