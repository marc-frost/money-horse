import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreditFormComponent} from './components/credit-item/credit-form.component';
import {CreditListComponent} from './components/credit-list/credit-list.component';
import {IdResolver} from '../+core/id.resolver';

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: CreditListComponent},
      {path: ':id', component: CreditFormComponent, resolve: {id: IdResolver}}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditRoutingModule {
}
