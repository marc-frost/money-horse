import {ValueGetterParams} from 'ag-grid-community/dist/lib/entities/colDef';
import {ColDef, ColGroupDef} from 'ag-grid-community';

export const columnDefs:  (ColDef | ColGroupDef)[] = [
  {
    field: 'month',
    valueGetter: (params: ValueGetterParams) => {
      return params.data.month + 1
    },
    type: 'rightAligned',
    width: 120,
  },
  {
    field: 'start',
    cellRenderer: 'dateCellRenderer',
    width: 120,
    initialPinned: 'left',
  },
  {
    field: 'rate',
    cellRenderer: 'currenyCellRenderer',
    type: 'rightAligned',
    width: 160,
  },
  {
    field: 'interestShare',
    cellRenderer: 'currenyCellRenderer',
    type: 'rightAligned',
    width: 160,
  },
  {
    field: 'repaymentShare', cellRenderer: 'currenyCellRenderer',
    type: 'rightAligned',
    width: 160,
  },
  {
    field: 'remainingDebt', cellRenderer: 'currenyCellRenderer',
    type: 'rightAligned',
    width: 160
  },
  {
    field: 'additionals',
    cellRenderer: 'additionalsCellRenderer',
    type: 'rightAligned',
    autoHeight: true
  }
];
