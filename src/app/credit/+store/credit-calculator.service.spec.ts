import {CreditCalculator} from "./credit-calculator.service";
import {Credit} from "./credit.model";
import dayjs from "dayjs";

describe('CreditCalculator', () => {

  let service: CreditCalculator;

  beforeEach(() => {
    service = new CreditCalculator();
  });

  it('credit 1000, 2% interest, 5% repayment', () => {
    const credit: Credit = {
      id: 't1',
      inputs: {
        name: 'Credit 1',
        start: dayjs("2022-01-01T00:00:00Z"),
        sum: 1000,
        interest: 2,
        repayment: 5,
      }
    };
    expect(service.calculate(credit)).toMatchSnapshot();
  });

  it('credit 1000, 2% interest, 5% repayment, addional payment after 1 year', () => {
    const credit: Credit = {
      id: 't1',
      inputs: {
        name: 'Credit 1',
        start: dayjs("2022-01-01T00:00:00Z"),
        sum: 1000,
        interest: 2,
        repayment: 5,
      },
      additional: [
        {
          start: dayjs("2023-01-01T00:00:00Z"),
          amount: 500
        }
      ]
    };
    expect(service.calculate(credit)).toMatchSnapshot();
  });
});
