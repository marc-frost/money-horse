import {createStore} from '@ngneat/elf';
import {
  addEntities,
  deleteEntities, getEntity,
  selectAllEntities,
  selectEntity,
  updateEntities, upsertEntities, upsertEntitiesById,
  withActiveId,
  withEntities
} from '@ngneat/elf-entities';
import {Credit, CreditAdditional, CreditUserInputs, copyCredit} from './credit.model';
import {Injectable} from '@angular/core';
import {localStorageStrategy, persistState} from '@ngneat/elf-persist-state';
import {CreditCalculator} from './credit-calculator.service';
import {map} from 'rxjs';
import dayjs from 'dayjs';
import {sortBy} from '../../+core/utils';

const store = createStore(
  {name: 'credits'},
  withEntities<Credit>(),
  withActiveId()
);

//export const store = new Store({name: 'credits', state, config});
export const persist = persistState(store, {
  key: 'credits',
  storage: localStorageStrategy,
});

@Injectable()
export class CreditRepository {

  all$ = store.pipe(
    selectAllEntities(),
    map(x => sortBy<Credit>(x, x => x.inputs?.name))
  );

  id = (id: string) => store.pipe(selectEntity(id));

  constructor(private creditCalculator: CreditCalculator) {
  }

  calc(id: string[]): void {
    store.update(
      updateEntities(id, (entity) => (this.creditCalculator.calculate({...entity})))
    );
  }

  copy(ids: string[]): void {
    for (const id of ids) {
      const current = store.query(getEntity(id));
      if(!current) continue;
      const copy = copyCredit(current);
      store.update(addEntities([copy]));
    }
  }

  update(id: string, inputs: CreditUserInputs, additional: CreditAdditional[]): void {
    store.update(upsertEntities({id: id}));
    store.update(updateEntities(id, (entity) => {
      if (entity?.inputs?.repayment !== inputs.repayment) {
        inputs.rate = undefined;
      } else if (entity?.inputs?.rate !== inputs.rate) {
        inputs.repayment = undefined;
      }
      return this.creditCalculator.calculate({...entity, inputs, additional});
    }));
  }

  updateStart(ids: string[], start: dayjs.Dayjs): void {
    store.update(
      updateEntities(ids, (entity) => (this.creditCalculator.calculate({
        ...entity,
        inputs: {...entity.inputs, start} as CreditUserInputs
      })))
    );
  }

  remove(ids: string[]): void {
    store.update(deleteEntities(ids));
  }
}
