import {Injectable} from '@angular/core';
import {
  createCreditUserInputs,
  Credit,
  CreditAdditional,
  CreditLine,
  CreditTotal,
  CreditUserInputs
} from './credit.model';
import dayjs from 'dayjs'

export function roundFinancial(number: number): number {
  return Math.round((number + Number.EPSILON) * 100) / 100;
}

export function findCurrent(date: dayjs.Dayjs, items: CreditLine[]): CreditLine | undefined {
  return items.find(x => date.isSame(x.start, 'year') && date.isSame(x.start, 'month'));
}

@Injectable()
export class CreditCalculator {

  public calculate(credit: Credit, withSavings = true): Credit {
    const result = {
      ...credit,
    };

    result.inputs = this.validateUserInputs(result);
    result.lines = this.calculateLines(result);
    result.totals = this.calculateTotals(result);

    // calculate savings
    if (withSavings) {

      const linesWithAdditionals = result.lines.filter(x => x.additionals);
      const additionalStack: CreditAdditional[] = [];
      const ref = this.calculate({
        ...result,
        lines: [],
        totals: undefined,
        additional: additionalStack,
      }, false);

      let totalSaving = (ref?.totals?.interest ?? 0);

      for (const linesWithAdditional of linesWithAdditionals) {
        additionalStack.push({
          start: linesWithAdditional.start,
          amount: linesWithAdditional.additionals ?? 0,
        });

        const copy: Credit = {
          ...result,
          lines: [],
          totals: undefined,
          additional: additionalStack,
        }
        const calcUntilThisMonth = this.calculate(copy, false);
        linesWithAdditional.savings = roundFinancial(totalSaving - (calcUntilThisMonth?.totals?.interest ?? 0));
        totalSaving = totalSaving - linesWithAdditional.savings;
      }

      result.totals.savings = roundFinancial(result.lines.map(x => x.savings ?? 0).reduce((sum: number, current: number) => sum + current, 0));
    }

    return result;
  }


  private validateUserInputs(credit: Credit): CreditUserInputs {
    if (!credit.inputs) return createCreditUserInputs({});
    const result = {
      ...credit.inputs
    }

    if (!result.rate && result.repayment) {
      result.rate = roundFinancial(
        (((result?.sum ?? 0) / 100) * (result?.interest ?? 1) / 12)
        + (((result?.sum ?? 0) / 100) * (result?.repayment ?? 1) / 12)
      )
      result.rate = this.rate(result.sum, result.interest, result.repayment);
    } else if (result.rate && result.interest && !result.repayment) {
      const replaymentPart = result.rate - ((result.sum ?? 0) * result.interest / 100 / 12);
      const replaymentRate = (100 * 12 * replaymentPart) / (result?.sum ?? 1);
      result.repayment = +(replaymentRate.toFixed(4));
    }

    return result;
  }

  private rate(sum?: number, interest?: number, repayment?: number): number {
    return roundFinancial((((sum ?? 0) / 100) * (interest ?? 1) / 12) + (((sum ?? 0) / 100) * (repayment ?? 1) / 12));
  }

  private calculateLines(credit: Credit): CreditLine[] {

    const result: CreditLine[] = [];
    const inputs = credit.inputs;

    if (!inputs || !inputs.sum || !inputs.interest || (!inputs.repayment && !inputs.rate)) return [];

    let total = inputs?.sum ?? 1;
    let repayment = 0;

    let currentInterest = inputs.interest || 1;
    let currentRepayment = inputs.repayment || 1;
    let currentRate = inputs.rate || 1;

    let i = 0;
    while (total > 0) {

      const date = dayjs(dayjs(inputs.start).add(i, 'month').format('YYYY-MM-DD'), {utc: true});

      let totalAdditionals = undefined;
      const additionals = credit.additional?.find(x => date.isSame(x.start, 'year') && date.isSame(x.start, 'month'));
      if (additionals) {
        if (additionals.interest) {
          currentInterest = additionals.interest;
        }
        if(additionals.repayment) {
          currentRepayment = additionals.repayment;
        }
        if (additionals.amount) {
         totalAdditionals = roundFinancial((totalAdditionals ?? 0) + (additionals.amount ?? 0));
        }
      }

      const interestRate = roundFinancial(roundFinancial(total / 100) * (currentInterest ?? 1) / 12);
      currentRate = this.rate(inputs.sum, currentInterest, currentRepayment);
      let repaymentRate = roundFinancial(currentRate - interestRate);

      total = roundFinancial(total - repaymentRate);

      if (totalAdditionals) {
         total = roundFinancial(total - totalAdditionals);
      }

      // const additionals = credit.additional?.filter(x => date.isSame(x.start, 'year') && date.isSame(x.start, 'month')) ?? [];
      // for (const additional of additionals) {
      //   totalAdditionals = roundFinancial((totalAdditionals ?? 0) + (additional.amount ?? 0));
      // }
      //
      // if (totalAdditionals) {
      //   total = roundFinancial(total - totalAdditionals);
      // }

      // last rate
      if (total < 0) {
        repaymentRate = roundFinancial((inputs?.sum ?? 0) - repayment);
        currentRate = repaymentRate;
        total = 0;
      }

      repayment = repayment + repaymentRate + (totalAdditionals ?? 0);

      result.push({
        start: date,
        remainingDebt: total,
        interestShare: interestRate,
        repaymentShare: repaymentRate,
        additionals: totalAdditionals,
        rate: currentRate,
        month: i++,
      });

      if (i > 1000) break;
    }

    return result;
  }

  private calculateTotals(x: Credit): CreditTotal {
    if (!x.lines || x.lines.length === 0) return {};

    const item: CreditTotal = {
      interest: roundFinancial(x.lines.map(x => x.interestShare ?? 0).reduce((sum: number, current: number) => sum + current, 0)),
      repayment: roundFinancial(x.lines.map(x => x.repaymentShare ?? 0).reduce((sum: number, current: number) => sum + current, 0)),
      additional: roundFinancial(x.additional?.map(x => x.amount ?? 0).reduce((sum: number, current: number) => sum + current, 0) ?? 0),
      lastRateDate: x.lines.slice(-1)[0].start,
      lastRateDateString: `${x.lines.slice(-1)[0].start}`,
    };

    item.sum = roundFinancial((x?.inputs?.sum ?? 0) + (item?.interest ?? 0));
    return item;
  }

}
