import {findCurrent} from './credit-calculator.service';
import dayjs from 'dayjs';
import {ValueGetterParams} from 'ag-grid-community/dist/lib/entities/colDef';
import {ColDef, ColGroupDef} from 'ag-grid-community';

export const columnDefs: (ColDef | ColGroupDef)[] = [
  {
    field: 'default',
    openByDefault: true,
    children: [
      {
        field: 'inputs.name',
        initialPinned: 'left',
        flex: 1,
        checkboxSelection: true,
        headerCheckboxSelection: true,
      },
      {
        field: 'inputs.start',
        cellRenderer: 'dateCellRenderer',
        minWidth: 120,
        initialPinned: 'left',
      },
      {
        field: 'inputs.sum',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        minWidth: 160,
        initialPinned: 'left',
      },
      {
        field: 'remainingDebt',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        minWidth: 160,
        initialPinned: 'left',
        valueGetter: (params: ValueGetterParams) => {
          console.log('ppp', params)
          if (params.node?.rowPinned) {
            return params.data.totals.repaymentDebt;
          }
          return findCurrent(dayjs(), params.data.lines)?.remainingDebt ?? 0
        }
      }]
  },
  {
    field: 'credit',
    openByDefault: true,
    children: [

      {
        field: 'inputs.interest',
        cellRenderer: 'percentCellRenderer',
        type: 'rightAligned',
        columnGroupShow: 'open',
        minWidth: 120
      },
      {
        field: 'inputs.repayment',
        cellRenderer: 'percentCellRenderer',
        type: 'rightAligned',
        columnGroupShow: 'open',
        minWidth: 140
      },
      {
        field: 'inputs.rate',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        columnGroupShow: 'open',
        minWidth: 120
      },
    ]
  },
  {
    field: 'calculated.totals',
    openByDefault: false,
    children: [
      {
        field: 'totals.sum',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        minWidth: 220
      },
      {
        field: 'totals.repayment',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        columnGroupShow: 'open',
        minWidth: 220
      },
      {
        field: 'totals.additional',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        columnGroupShow: 'open',
        minWidth: 220
      },
      {
        field: 'totals.interest',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        columnGroupShow: 'open',
        minWidth: 120
      },
      {
        field: 'totals.additional',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        columnGroupShow: 'open',
        minWidth: 120
      },
      {
        field: 'totals.savings',
        cellRenderer: 'currenyCellRenderer',
        type: 'rightAligned',
        columnGroupShow: 'open',
        minWidth: 120
      },
      {
        field: 'totals.lastRateDate',
        cellRenderer: 'dateCellRenderer',
        minWidth: 120,
        columnGroupShow: 'open'
      },
    ]
  }
];
