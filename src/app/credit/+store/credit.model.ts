import dayjs from 'dayjs';
import {Dayjs} from 'dayjs';
import {v4 as uuid} from 'uuid';

export type Credit = {
  id: string;
  inputs: CreditUserInputs;
  lines?: CreditLine[];
  additional?: CreditAdditional[];
  totals?: CreditTotal;
};

export type CreditUserInputs = {
  name: string;
  tags?: string[];
  sum: number | undefined;
  start?: undefined | Dayjs | Date | string;
  interest?: number | undefined;
  repayment?: number | undefined;
  rate?: number | undefined;
};

export type CreditTotal = {
  sum?: number | undefined;
  interest?: number | undefined;
  repayment?: number | undefined;
  repaymentDebt?: number | undefined;
  rateCount?: number | undefined;
  additional?: number | undefined;
  savings?: number | undefined;
  lastRateDate?: Dayjs;
  lastRateDateString?: string;
};

export type CreditLine = {
  start: Dayjs;
  month: number;
  remainingDebt?: number | undefined;
  interestShare?: number | undefined;
  repaymentShare?: number | undefined;
  additionals?: number | undefined;
  savings?: number | undefined;
  rate?: number | undefined;
}

export type CreditAdditional = {
  start: Dayjs;
  amount: number;
  interest?: number | undefined;
  repayment?: number | undefined;
}

export function createCredit(x: Partial<Credit>): Credit {
  return {
    id: uuid(),
    inputs: createCreditUserInputs({}),
    lines: [],
    additional: [],
    ... x,
  } as Credit;
}

export function copyCredit(x: Partial<Credit>): Credit {
  return {
    ... x,
    id: uuid(),
    inputs: {
      ... (x.inputs || createCreditUserInputs({})),
      name: `${x.inputs?.name} copy`,
    }
  }
}

export function createCreditUserInputs(x: Partial<CreditUserInputs>): CreditUserInputs {
  return {
    name: '',
    start: dayjs(),
    sum: 1000,
    interest: 1,
    repayment: 3,
    tags: [],
    ...x,
  } as CreditUserInputs;
}
