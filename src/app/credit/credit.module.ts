import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreditFormComponent} from './components/credit-item/credit-form.component';
import {CreditRoutingModule} from './credit-routing.module';
import {CreditListComponent} from './components/credit-list/credit-list.component';
import {TRANSLOCO_SCOPE, TranslocoModule} from '@ngneat/transloco';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatBadgeModule} from '@angular/material/badge';
import {MatToolbarModule} from '@angular/material/toolbar';
import {AdditionalsCellRendererComponent} from './components/credit-item/components/additionals-cell-renderer.component';
import {AgGridWrapperModule} from '../+share/ag-grid-wrapper/ag-grid-wrapper.module';
import {CreditRepository} from './+store/credit-repository.service';
import {CreditCalculator} from './+store/credit-calculator.service';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [
    CreditFormComponent,
    CreditListComponent,
    AdditionalsCellRendererComponent
  ],
  imports: [
    CreditRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    TranslocoModule,
    MatCardModule,
    MatChipsModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatBadgeModule,
    AgGridWrapperModule,
  ],
  providers: [
    {provide: TRANSLOCO_SCOPE, useValue: 'credit'},
    CreditRepository,
    CreditCalculator
  ]
})
export class CreditModule {
}
