import {Component} from '@angular/core';
import {CreditRepository} from '../../+store/credit-repository.service';
import {of, tap} from 'rxjs';
import {createCredit, Credit} from '../../+store/credit.model';
import {UntypedFormControl} from '@angular/forms';
import dayjs from 'dayjs';
import {Api} from '../../../+share/ag-grid-wrapper/ag-grid-wrapper.module';
import {columnDefs} from '../../+store/credit-list.grid';
import {sum} from '../../../+core/utils';
import {findCurrent} from '../../+store/credit-calculator.service';

@Component({
  selector: 'app-credit-list',
  templateUrl: './credit-list.component.html',
  styleUrls: ['./credit-list.component.scss']
})
export class CreditListComponent {
  pinnedBottomRowData: Credit[] = [];
  all$ = this.repo.all$.pipe(
    tap((x: Credit[]) => {
      this.pinnedBottomRowData = [
        createCredit({
          inputs: {
            name: '',
            sum: sum<Credit>(x, x => x.inputs?.sum),
            rate: sum<Credit>(x, x => x.inputs?.rate),
          },
          totals: {
            sum: sum<Credit>(x, x => x.totals?.sum),
            savings: sum<Credit>(x, x => x.totals?.savings),
            repayment: sum<Credit>(x, x => x.totals?.repayment),
            interest: sum<Credit>(x, x => x.totals?.interest),
            additional: sum<Credit>(x, x => x.totals?.additional),
            repaymentDebt: sum<Credit>(x, x => {
              return findCurrent(dayjs(), (x?.lines || []))?.remainingDebt;
            }),
          }
        })
      ];
    }),
  );

  columnDefs$ = of(columnDefs);
  api: Api | undefined;

  startForm = new UntypedFormControl();
  startLoader$ = this.startForm.valueChanges.pipe(
    tap(x => {
      const ids = (this?.api?.getSelectedRows() ?? []).map(x => x.id);
      this.repo.updateStart(ids, dayjs(x));
    })
  ).subscribe();

  constructor(private repo: CreditRepository) {
  }

  onApi($event: Api): void {
    this.api = $event;
  }

  calc(): void {
    const ids = (this?.api?.getSelectedRows() ?? []).map(x => x.id);
    this.repo.calc(ids);
  }

  copy(): void {
    const ids = (this?.api?.getSelectedRows() ?? []).map(x => x.id);
    this.repo.copy(ids);
  }

  remove(): void {
    const ids = (this?.api?.getSelectedRows() ?? []).map(x => x.id);
    this.repo.remove(ids);
  }
}
