import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslocoTestingModule} from "@ngneat/transloco";
import {CreditListComponent} from "./credit-list.component";
import {CreditRepository} from "../../+store/credit-repository.service";
import {CreditCalculator} from "../../+store/credit-calculator.service";
import {defaultConfig} from '../../../+core/test-helper';

describe('CreditListComponent', () => {
  let component: CreditListComponent;
  let fixture: ComponentFixture<CreditListComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})
      ],
      declarations: [CreditListComponent],
      providers: [CreditRepository, CreditCalculator]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
