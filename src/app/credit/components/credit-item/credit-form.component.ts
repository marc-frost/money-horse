import {Component} from '@angular/core';
import {CreditRepository} from '../../+store/credit-repository.service';
import {BehaviorSubject, debounceTime, map, of, pluck, switchMap, tap} from 'rxjs';
import {Credit, CreditAdditional, CreditLine, CreditUserInputs} from '../../+store/credit.model';
import {ActivatedRoute} from '@angular/router';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
//import {MatLegacyChipInputEvent as MatChipInputEvent} from '@angular/material/legacy-chips';
import {filterNil} from '@ngneat/elf';
import {CreditCalculator} from '../../+store/credit-calculator.service';
import {ControlsOf, FormControl, FormGroup} from '@ngneat/reactive-forms';
import dayjs from 'dayjs';
import {columnDefs} from '../../+store/credit-form.grid';
import {AdditionalsCellRendererComponent} from './components/additionals-cell-renderer.component';
import {MatChipInputEvent} from '@angular/material/chips';

@Component({
  selector: 'app-credit-item',
  templateUrl: './credit-form.component.html',
  styleUrls: ['./credit-form.component.scss']
})
export class CreditFormComponent {

  active = new BehaviorSubject<Credit | undefined>(undefined);
  active$ = this.active.asObservable();
  lines: CreditLine[] = [];

  columnDefs$ = of(columnDefs);

  tags: string[] = [];
  addition: CreditAdditional[] = [];
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  form = new FormGroup<ControlsOf<Credit>>({
    id: new FormControl(''),
    inputs: new FormGroup<ControlsOf<CreditUserInputs>>({
      name: new FormControl(''),
      sum: new FormControl(undefined),
      interest: new FormControl(undefined),
      repayment: new FormControl(undefined),
      rate: new FormControl(undefined),
      start: new FormControl(undefined)
    })
  });

  lastRateDate = new FormControl<Date | undefined>({value: undefined, disabled: true});

  additions = new FormGroup({
    amount: new FormControl<number>(undefined),
    start: new FormControl<Date>(undefined),
    interest: new FormControl<number>(undefined),
    repayment: new FormControl<number>(undefined),
  });

  loader$ = this.route.params.pipe(
    map((x:any) => x?.id),
    filterNil(),
    tap(x => (this.form.patchValue({id: x}))),
    switchMap(x => this.repository.id(x)),
    filterNil(),
    tap(x => {
      this.form.patchValue(x as never);
      this.tags = x.inputs?.tags ?? [];
      this.addition = x.additional ?? [];
      this.active.next(x);
    }),
    debounceTime(100),
    tap(() => this.calc())
  );
  loader = this.loader$.subscribe();
  frameworks = {
    'additionalsCellRenderer': AdditionalsCellRendererComponent
  };


  constructor(
    private repository: CreditRepository,
    private calculator: CreditCalculator,
    private route: ActivatedRoute
  ) {
  }

  addTag(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.tags.push(value);
    }
    event.chipInput?.clear();
  }

  removeTag(tag: string): void {
    const index = this.tags.indexOf(tag);
    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  addAdditional() {
    const value = this.additions.value;
    this.addition.push({
      start: dayjs(value.start),
      amount: value.amount,
      interest: value.interest,
      repayment: value.repayment,
    });
    this.additions.reset();
  }

  removeAdditional(item: CreditAdditional) {
    const index = this.addition.indexOf(item) ?? -1;
    if (index >= 0) {
      this.addition.splice(index, 1);
    }
  }

  calc(): void {
    const data = {
      ...this.form.value,
      additional: this.addition
    } as Credit;
    const result = this.calculator.calculate(data);
    if (result) {
      this.lines = result?.lines ?? [];
      this.lastRateDate.patchValue(result.totals?.lastRateDate?.toDate());
      this.form.patchValue({
        inputs: result?.inputs
      }, {emitEvent: false})
    }
  }

  onSubmit(): void {
    console.log(this.form)
    if (this.form.valid) {
      const value = {
        ...this.form.value,
        ...{
          inputs: {
            ...this.form.value.inputs,
            tags: this.tags
          }
        },
      };
      this.repository.update(value.id, value.inputs, this.addition)
    }
  }
}
