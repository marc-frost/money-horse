import {Component} from '@angular/core';
import {AgRendererComponent} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';

@Component({
  selector: 'app-additionals-cell-renderer',
  templateUrl: 'additionals-cell-renderer.component.html',
  styleUrls: ['./additionals-cell-renderer.component.scss']
})
export class AdditionalsCellRendererComponent implements AgRendererComponent {
  params: ICellRendererParams | undefined;

  agInit(params: ICellRendererParams): void {
    this.params = params;
  }

  refresh(params: ICellRendererParams): boolean {
    return !!params;
  }
}
