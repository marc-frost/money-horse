import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslocoTestingModule} from "@ngneat/transloco";
import {CreditFormComponent} from "./credit-form.component";
import {ActivatedRoute} from "@angular/router";
import {of} from "rxjs";
import {CreditRepository} from "../../+store/credit-repository.service";
import {CreditCalculator} from "../../+store/credit-calculator.service";
import {defaultConfig} from "../../../+core/test-helper";

describe('CreditFormComponent', () => {
  let component: CreditFormComponent;
  let fixture: ComponentFixture<CreditFormComponent>;

  const entity = {
    id: '0000-0000-0000-0000-0000',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})
      ],
      declarations: [CreditFormComponent], providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({id: entity.id}),
          }
        },
        CreditRepository,
        CreditCalculator
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
