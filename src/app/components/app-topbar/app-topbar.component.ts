import {Component, Input} from '@angular/core';
import {AppRepository} from '../../+store/app-repository.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './app-topbar.component.html',
  styleUrls: ['./app-topbar.component.scss']
})
export class AppTopbarComponent {

  locale$ = this.appRepository.locale$;
  @Input() small = false;

  constructor(
    private appRepository: AppRepository,
  ) {
  }

  switch(lang: string): void {
    this.appRepository.updateLocale(lang);
  }
}
