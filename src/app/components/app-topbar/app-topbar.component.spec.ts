import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslocoTestingModule} from "@ngneat/transloco";
import {AppTopbarComponent} from "./app-topbar.component";
import {defaultConfig} from "../../+core/test-helper";
import {AppRepository} from "../../+store/app-repository.service";

describe('AppTopbarComponent', () => {
  let component: AppTopbarComponent;
  let fixture: ComponentFixture<AppTopbarComponent>;
  let appRepository: AppRepository;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppTopbarComponent],
      imports: [
        TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})
      ],
      providers: [AppRepository]
    })
      .compileComponents();

    appRepository = TestBed.inject(AppRepository);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppTopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('switch locale', () => {
    jest.spyOn(appRepository, "updateLocale");
    component.switch("en");
    expect(appRepository.updateLocale).toHaveBeenCalledWith("en");
  });
});
