import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AppStartComponent} from './app-start.component';
import {TranslocoTestingModule} from "@ngneat/transloco";
import {defaultConfig} from '../../+core/test-helper';

describe('AppStartComponent', () => {
  let component: AppStartComponent;
  let fixture: ComponentFixture<AppStartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppStartComponent],
      imports: [
        TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
