import {ComponentFixture, TestBed} from '@angular/core/testing';

import {LayoutToolsComponent} from './layout-tools.component';
import {RouterTestingModule} from "@angular/router/testing";
import {AppTopbarComponent} from "../app-topbar/app-topbar.component";
import {TranslocoTestingModule} from "@ngneat/transloco";
import {defaultConfig} from '../../+core/test-helper';

describe('LayoutToolsComponent', () => {
  let component: LayoutToolsComponent;
  let fixture: ComponentFixture<LayoutToolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LayoutToolsComponent, AppTopbarComponent],
      imports: [RouterTestingModule, TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
