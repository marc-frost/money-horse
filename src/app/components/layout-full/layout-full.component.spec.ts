import {ComponentFixture, TestBed} from '@angular/core/testing';
import {LayoutFullComponent} from './layout-full.component';
import {RouterTestingModule} from "@angular/router/testing";
import {AppTopbarComponent} from "../app-topbar/app-topbar.component";
import {TranslocoTestingModule} from "@ngneat/transloco";
import {defaultConfig} from '../../+core/test-helper';

describe('LayoutFullComponent', () => {
  let component: LayoutFullComponent;
  let fixture: ComponentFixture<LayoutFullComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LayoutFullComponent, AppTopbarComponent],
      imports: [RouterTestingModule, TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
