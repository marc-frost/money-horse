import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoadingStateComponent} from './loading-state.component';
import {MatIconModule} from '@angular/material/icon';
import {MatLegacyProgressSpinnerModule as MatProgressSpinnerModule} from '@angular/material/legacy-progress-spinner';


@NgModule({
  declarations: [
    LoadingStateComponent
  ],
  exports: [
    LoadingStateComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatProgressSpinnerModule
  ]
})
export class LoadingStateModule {
}
