export enum LoadingStates {
  Initial,
  Loading,
  Finished,
  Error
}
