import {Component, Input} from '@angular/core';
import {LoadingStates} from './loadingStates';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-loading-state',
  templateUrl: './loading-state.component.html',
  styleUrls: ['./loading-state.component.scss']
})
export class LoadingStateComponent {

  states = LoadingStates;
  private tick = new BehaviorSubject<LoadingStates>(LoadingStates.Initial);
  tick$ = this.tick.asObservable();

  @Input() set state(value: LoadingStates | undefined | null) {
    if (value) {
      this.tick.next(value);
      setTimeout(() => this.tick.next(LoadingStates.Initial), 1000);
    }
  }
}
