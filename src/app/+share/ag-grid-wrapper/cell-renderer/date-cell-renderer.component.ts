import {Component} from '@angular/core';
import {AgRendererComponent} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';

@Component({
  selector: 'app-date-cell-renderer',
  templateUrl: 'date-cell-renderer.component.html'
})
export class DateCellRendererComponent implements AgRendererComponent {
  params: ICellRendererParams | undefined;

  agInit(params: ICellRendererParams): void {
    this.params = params;
  }

  refresh(params: ICellRendererParams): boolean {
    return !!params;
  }
}
