import {Component} from '@angular/core';
import {AgRendererComponent} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';

@Component({
  selector: 'app-curreny-cell-renderer',
  templateUrl: 'curreny-cell-renderer.component.html'
})
export class CurrenyCellRendererComponent implements AgRendererComponent {
  params: ICellRendererParams | undefined;

  agInit(params: ICellRendererParams): void {
    this.params = params;
  }

  refresh(params: ICellRendererParams): boolean {
    return !!params;
  }
}
