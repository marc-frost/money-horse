import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {
  ColDef,
  ColGroupDef,
  FirstDataRenderedEvent,
  GridApi,
  GridOptions,
  GridReadyEvent,
  RowDoubleClickedEvent
} from 'ag-grid-community';
import {CurrenyCellRendererComponent} from './cell-renderer/curreny-cell-renderer.component';
import {PercentCellRendererComponent} from './cell-renderer/percent-cell-renderer.component';
import {DateCellRendererComponent} from './cell-renderer/date-cell-renderer.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Api} from './ag-grid-wrapper.module';
import {BehaviorSubject, map} from 'rxjs';
import {HashMap} from '@ngneat/transloco';

declare type TranslateFn = (key: string, params?: HashMap) => string;

@Component({
  selector: 'app-ag-grid-wrapper',
  templateUrl: './ag-grid-wrapper.component.html',
  styleUrls: ['./ag-grid-wrapper.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AgGridWrapperComponent {

  @Input() pinnedBottomRowData: unknown[] = [];
  @Input() all: unknown[] | undefined | null = [];
  @Input() defaultColDef = {
    filter: true,
    sortable: true,
    resizable: true
  };
  frameworks = {
    'currenyCellRenderer': CurrenyCellRendererComponent,
    'percentCellRenderer': PercentCellRendererComponent,
    'dateCellRenderer': DateCellRendererComponent
  };
  @Input() gridOptions: GridOptions = {
    onRowDoubleClicked: (event: RowDoubleClickedEvent) => this.onRowDoubleClicked(event),
    context: this,
  };
  @Output() api = new EventEmitter<Api>();
  gridApi: GridApi | undefined;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  @Input() set frameworkComponents(value: { [p: string]: { new(): unknown } }) {
    this.frameworks = {
      ...this.frameworks,
      ...value,
    }
  }

  defs = new BehaviorSubject<(ColDef | ColGroupDef)[]>([]);

  @Input() set columnDefs(x: (ColDef | ColGroupDef)[] | undefined | null) {
    if (x) {
      this.defs.next(x);
    }
  }

  @Input() translate: TranslateFn = (key) => key;


  defs$ = this.defs.asObservable()
    .pipe(
      map(x => x.map((y: ColDef | ColGroupDef) => {

      const mapCol = (z: ColDef | ColGroupDef) => {
        const col = z as ColDef;
        if (col) {
            z.headerName = this.translate(col.field ?? '');
        }
      }
      mapCol(y);

      const colGroup = y as ColGroupDef;
      if (colGroup && colGroup.children) {
        colGroup.children.map(z => mapCol(z));
      }
      return y;
    })));

  onGridReady(params: GridReadyEvent) {
    this.gridApi = params.api;
    this.api.emit(params.api);
  }

  onFirstDataRendered($event: FirstDataRenderedEvent) {
    if ($event) {
      this.gridApi?.sizeColumnsToFit();
    }
  }

  onRowDoubleClicked($event: RowDoubleClickedEvent) {
    this.router.navigate([$event.data.id], {relativeTo: this.route});
  }
}
