import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AgGridWrapperComponent} from './ag-grid-wrapper.component';
import {TranslocoTestingModule} from "@ngneat/transloco";
import {RouterTestingModule} from "@angular/router/testing";
import {AgGridModule} from "ag-grid-angular";

describe('AgGridWrapperComponent', () => {
  let component: AgGridWrapperComponent;
  let fixture: ComponentFixture<AgGridWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslocoTestingModule, RouterTestingModule, AgGridModule],
      declarations: [AgGridWrapperComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgGridWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
