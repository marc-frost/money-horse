import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AgGridWrapperComponent} from './ag-grid-wrapper.component';
import {AgGridModule} from 'ag-grid-angular';
import {CurrenyCellRendererComponent} from './cell-renderer/curreny-cell-renderer.component';
import {PercentCellRendererComponent} from './cell-renderer/percent-cell-renderer.component';
import {DateCellRendererComponent} from './cell-renderer/date-cell-renderer.component';
import {GridApi} from 'ag-grid-community';

export class Api extends GridApi {
}

@NgModule({
  declarations: [
    AgGridWrapperComponent,
    CurrenyCellRendererComponent,
    PercentCellRendererComponent,
    DateCellRendererComponent
  ],
  exports: [
    AgGridWrapperComponent
  ],
  imports: [
    CommonModule,
    AgGridModule
  ]
})
export class AgGridWrapperModule {
}
