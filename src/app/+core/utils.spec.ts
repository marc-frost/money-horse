import {sort, sortBy, sum} from "./utils";

describe('utils tests', () => {

  const items = [
    {name: "A Demo", count: 5},
    {name: "a Demo", count: 2},
    {name: "4 Demo", count: 1},
    {name: "C Demo", count: 20},
    {name: "_ Demo", count: undefined},
    {name: undefined, count: undefined},
    {name: undefined, count: 5}
  ];

  it('sortBy name', () => {
    const result = sortBy(items, x => x.name);
    expect(result).toMatchSnapshot();
  });

  it('sortBy name desc', () => {
    const result = sortBy(items, x => x.name, sort.DESC);
    expect(result).toMatchSnapshot();
  });

  it('sortBy count', () => {
    const result = sortBy(items, x => x.count);
    expect(result).toMatchSnapshot();
  });

  it('sortBy count desc', () => {
    const result = sortBy(items, x => x.count, sort.DESC);
    expect(result).toMatchSnapshot();
  });

  it('sum test', () => {
    const result = sum(items, x => x.count);
    expect(result).toBe(33)
  });

  it('sum test without data', () => {
    const result = sum<{ count: number }>(undefined, x => x.count);
    expect(result).toBe(0)
  });
});
