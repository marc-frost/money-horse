export enum sort {
  ASC,
  DESC
}

export const sortBy = <T>(x: T[], p: (x: T) => string | number | undefined, sortDirection = sort.ASC): T[] =>
  x.sort((a, b) => {
    const compare = `${p(a) ?? ''}`
      .localeCompare(`${p(b) ?? ''}`, undefined, {
        numeric: true,
        sensitivity: 'base'
      }) ?? 0;
    return sortDirection === sort.ASC ? compare : compare * -1;
  });

export const sum = <T>(values: T[] | undefined, p: (x: T) => number | undefined) => (values ?? [])
  .map(x => p(x) ?? 0).reduce((sum, current) => sum + current, 0);
