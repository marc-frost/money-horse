import {IdResolver} from "./id.resolver";
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from "@angular/router";

describe('IdResolver', () => {
  let resolver: IdResolver;
  let router: Router = {} as Router;
  let activatedRouteSnapshot: ActivatedRouteSnapshot;
  let routerStateSnapshot: RouterStateSnapshot

  beforeEach(() => {
    activatedRouteSnapshot = {paramMap: {}} as ActivatedRouteSnapshot;
    routerStateSnapshot = {} as RouterStateSnapshot;
    router.navigateByUrl = jest.fn();
    resolver = new IdResolver(router);
  });

  it('intital', (done) => {
    expect(resolver).toBeTruthy();
    done();
  });

  it('route with empty id', (done) => {

    activatedRouteSnapshot.paramMap.get = () => null;
    routerStateSnapshot.url = "demo";

    resolver.resolve(activatedRouteSnapshot, routerStateSnapshot).subscribe(x => {
      expect(x).toHaveLength(36);
      done();
    })
  });

  it('route with guid as id', (done) => {

    activatedRouteSnapshot.paramMap.get = () => "cbacf0bb-021b-4e41-946a-337a0bccdd66";
    routerStateSnapshot.url = "demo";

    resolver.resolve(activatedRouteSnapshot, routerStateSnapshot).subscribe(x => {
      expect(x).toHaveLength(36);
      done();
    })
  });

  it('route with new as id', (done) => {

    activatedRouteSnapshot.paramMap.get = () => "new";
    routerStateSnapshot.url = "demo";

    resolver.resolve(activatedRouteSnapshot, routerStateSnapshot).subscribe(() => {
      expect(router.navigateByUrl).toHaveBeenCalledTimes(1);
      done();
    })
  });
});
