import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {v4 as uuid} from 'uuid';

@Injectable({providedIn: 'root'})
export class IdResolver  {

  constructor(private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {
    const id = route.paramMap.get('id') ?? 'new';
    const isNew = 'new'.localeCompare(id, undefined, {sensitivity: 'accent'}) === 0;
    if (isNew) {
      const target = `${state.url.substring(0, state.url.length - 3)}${uuid()}`;
      this.router.navigateByUrl(target);
    }
    return of(isNew ? uuid() : id);
  }
}
