import {TranslocoConfig} from '@ngneat/transloco';

export const defaultConfig: Partial<TranslocoConfig> = {
  missingHandler: {
    logMissingKey: false
  }
}
