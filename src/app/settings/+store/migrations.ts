import {add_residential_units_id} from './migrations/11_add_residential_units_id';
import {delete_credit_state} from './migrations/10_delete_credit_state';

export const Version = '1.1';

export function createFileWithVersion(key: string, data: unknown): FileWithVersion {
  return {
    version: '0.1',
    key,
    data
  };
}

export type FileWithVersion = {
  version: string,
  key: string,
  data: unknown;
};

const migrations = [
  delete_credit_state,
  add_residential_units_id,
];

export const migrate = <T>(key: string, version: string, data: T) => {
  const versionNumber = Number(version);
  const filteredMigrations = migrations
    .filter(x => x.key === key || x.key === undefined)
    .filter(x => versionNumber < Number(x.version))
    .sort((a, b) => a.version.localeCompare(b.version, undefined, {numeric: true, sensitivity: 'base'}))
  ;
  let result = data;
  for (const migration of filteredMigrations) {
    result = migration.up(result);
  }
  return result;
};
