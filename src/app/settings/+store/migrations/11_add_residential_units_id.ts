import {v4 as uuid} from 'uuid';
import {StoreKeys} from '../store.keys';

const fixAll = (before: any) => {
  return fixUndefined(fixResidentialUnitId(before));
}

const fixUndefined = (before: any) => {
  const properties: Record<string, any> = {};
  for (const [key, e] of Object.entries(before.data.entities)) {
    if (key !== undefined && key !== 'undefined') {
      properties[key] = e;
    }
  }
  before.data.entities = properties;
  delete before.data.entity;
  return before;
};

const fixResidentialUnitId = (before: any) => {
  for (const [key, e] of Object.entries(before.data.entities)) {
    if (e && (e as any).residentialUnits) {
      for (const r of (e as any).residentialUnits) {
        if (r.id === '') {
          r.id = uuid();
        }
      }
    }
  }
  return before;
};

export const add_residential_units_id = {
  version: '1.1',
  key: StoreKeys.PROPERTY,
  up: (before: any) => ({
    ...fixAll(before),
    version: '1.1',
  }),
};
