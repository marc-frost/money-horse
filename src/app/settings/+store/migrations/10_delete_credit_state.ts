import {StoreKeys} from '../store.keys';

const deleteState = (before: any) => {
  for (const [key, e] of Object.entries(before.data.entities)) {
    delete (e as any).state;
  }
  return before;
};

export const delete_credit_state = {
  version: '1.0',
  key: StoreKeys.CREDITS,
  up: (before: any) => ({
    ...deleteState(before),
    version: '1.0',
  }),
};
