import {Injectable} from '@angular/core';
import {createFileWithVersion, migrate} from './migrations';
import {StoreKeys} from './store.keys';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  getKeys = () => [StoreKeys.CREDITS, StoreKeys.PROPERTY];

  getSettings(): Map<string, string> {
    const settings = new Map<string, string>();
    for (const key of this.getKeys()) {
      const storedData = JSON.parse(localStorage.getItem(key) || '{}');
      const fileWithVersion = JSON.stringify(createFileWithVersion(key, storedData), null, 2);
      settings.set(key, fileWithVersion);
    }
    return settings;
  }

  setSettings(settings: Map<string, string>): void {
    for (const [key, value] of settings) {
      const storeWithVersion = JSON.parse(value);
      const migratedData = migrate(key, storeWithVersion.version, storeWithVersion);
      const storeData = JSON.stringify(migratedData.data);
      localStorage.setItem(key, storeData);
    }
    window.location.reload();
  }
}
