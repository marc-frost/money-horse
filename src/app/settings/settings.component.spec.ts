import {ComponentFixture, TestBed} from '@angular/core/testing';
import {SettingsComponent} from './settings.component';
import {RouterTestingModule} from "@angular/router/testing";
import {TranslocoTestingModule} from "@ngneat/transloco";
import {defaultConfig} from '../+core/test-helper';
import {S3Module} from "./components/s3/s3.module";

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, S3Module, TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})],
      declarations: [SettingsComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
