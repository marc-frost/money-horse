import {Component} from '@angular/core';
import {ControlsOf, FormControl, FormGroup} from '@ngneat/reactive-forms';
import {S3Repository, S3Settings} from './+store/s3.repository.service';
import {S3ClientService} from './+store/s3-client.service';
import {combineLatest, take, tap} from 'rxjs';
import {SettingsService} from '../../+store/settings.service';

@Component({
  selector: 'app-settings-storage-s3',
  templateUrl: './s3.component.html',
  styleUrls: ['./s3.component.scss'],
  providers: []
})
export class S3Component {

  form = new FormGroup<ControlsOf<S3Settings>>({
    accessKeyId: new FormControl(''),
    secretAccessKey: new FormControl(''),
    bucket: new FormControl(''),
    region: new FormControl(''),
  });

  loader$ = this.s3Repository.current$
    .pipe(
      take(1),
      tap(x => this.form.patchValue(x))
    ).subscribe();

  checkingState$ = this.s3Repository.pluck('checkingState');
  uploadState$ = this.s3Repository.pluck('uploadState');
  downloadState$ = this.s3Repository.pluck('downloadingState');
  versioningState$ = this.s3Repository.pluck('versioningState');
  isLoading$ = this.s3Repository.isLoading$;

  constructor(
    private s3client: S3ClientService,
    private s3Repository: S3Repository,
    private settingsService: SettingsService,
  ) {
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.s3Repository.update(this.form.value);
    }
  }

  check(): void {
    const settings = this.form.value as S3Settings;
    this.s3client.validate(settings).subscribe();
  }

  upload(): void {

    const settings = this.form.value as S3Settings;
    const multi = [];
    for (const [key, value] of this.settingsService.getSettings()) {
      multi.push(this.s3client.write(settings, `${key}.json`, value));
    }
    combineLatest(multi).subscribe();
  }

  download(): void {
    const settings = this.form.value as S3Settings;
    const multi = [];
    for (const key of this.settingsService.getKeys()) {
      multi.push(this.s3client.read(settings, `${key}.json`));
    }

    combineLatest(multi).pipe(
      tap((x: { fileName: string, content: string }[]) => {
        const settings = new Map<string, string>();
        x.map(y => {
          const key = y.fileName.substring(0, y.fileName.length - 5);
          settings.set(key, y.content);
        });
        this.settingsService.setSettings(settings);
      }),
    ).subscribe();
  }
}
