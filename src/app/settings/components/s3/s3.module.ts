import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {S3Component} from './s3.component';
import {MatIconModule} from '@angular/material/icon';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslocoModule} from '@ngneat/transloco';
import {MatDividerModule} from '@angular/material/divider';
import {LoadingStateModule} from '../../../+share/loading-state/loading-state.module';
import {MatBadgeModule} from '@angular/material/badge';
import {S3ClientService} from './+store/s3-client.service';
import {S3Repository} from './+store/s3.repository.service';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [
    S3Component
  ],
  exports: [
    S3Component
  ],
  providers: [
    S3ClientService,
    S3Repository
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    TranslocoModule,
    MatDividerModule,
    LoadingStateModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatBadgeModule
  ]
})
export class S3Module {
}
