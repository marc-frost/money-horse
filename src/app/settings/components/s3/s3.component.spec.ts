import {ComponentFixture, TestBed} from '@angular/core/testing';

import {S3Component} from './s3.component';
import {S3Repository} from "./+store/s3.repository.service";
import {RouterTestingModule} from "@angular/router/testing";
import {TranslocoTestingModule} from "@ngneat/transloco";
import {defaultConfig} from '../../../+core/test-helper';
import {S3ClientService} from "./+store/s3-client.service";

describe('S3Component', () => {
  let component: S3Component;
  let fixture: ComponentFixture<S3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslocoTestingModule.forRoot({translocoConfig: defaultConfig})],
      declarations: [S3Component],
      providers: [S3Repository, S3ClientService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(S3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
