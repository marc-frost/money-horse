import {S3ClientService} from "./s3-client.service";
import {S3Repository} from "./s3.repository.service";

describe('S3 Client Service', () => {
  let client: S3ClientService;
  let s3Repository: S3Repository;

  const settings = {
    accessKeyId: process.env["S3_ACCESS_KEY_ID"],
    secretAccessKey: process.env["S3_SECRET_ACCESS_KEY"],
    bucket: process.env["S3_BUCKET"],
    region: 'eu-central-1'
  };

  beforeEach(async () => {
    s3Repository = new S3Repository();
    client = new S3ClientService(s3Repository);
  });

  it('validate test', done => {
    const result = client.validate(settings);
    result.subscribe(x => {
      expect(x).toBeTruthy();
      done();
    })
  });

  it('write test', done => {
    const result = client.write(settings, 'unittest.json', "{demo: '123'}");
    result.subscribe(x => {
      expect(x).toBeTruthy();
      done();
    })
  });

  it('read test', done => {
    const result = client.read(settings, 'unittest.json');
    result.subscribe(x => {
      expect(x).toBeTruthy();
      done();
    })
  });
});
