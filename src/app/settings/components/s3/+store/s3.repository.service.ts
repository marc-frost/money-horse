import {createState, select, Store, withProps} from '@ngneat/elf';
import {localStorageStrategy, persistState} from '@ngneat/elf-persist-state';
import {Injectable} from '@angular/core';
import dayjs from 'dayjs';
import {LoadingStates} from '../../../../+share/loading-state/loadingStates';
import {pluck} from 'rxjs';

export type S3Settings = {
  accessKeyId?: string;
  secretAccessKey?: string;
  bucket?: string;
  region?: string;

  lastCheck?: dayjs.Dayjs | null,
  versioningState?: boolean | null;

  checkingState?: LoadingStates | undefined;
  downloadingState?: LoadingStates | undefined;
  uploadState?: LoadingStates | undefined;
}

const {state, config} = createState(
  withProps<S3Settings>({
    checkingState: LoadingStates.Initial,
    downloadingState: LoadingStates.Initial,
    uploadState: LoadingStates.Initial,
  }),
);

export const store = new Store({name: 's3Settings', state, config});
persistState(store, {
  key: 's3Settings',
  storage: localStorageStrategy,
  preStoreInit: (value) => ({
    ...value,
    checkingState: LoadingStates.Initial,
    downloadingState: LoadingStates.Initial,
    uploadState: LoadingStates.Initial,
  })
});

@Injectable()
export class S3Repository {
  current$ = store.pipe();
  isLoading$ = store.pipe(
    select(x =>
      x.downloadingState === LoadingStates.Loading
      || x.uploadState === LoadingStates.Loading
      || x.checkingState === LoadingStates.Loading)
  );
  pluck = <K1 extends keyof S3Settings>(prop: K1) => store.pipe(pluck(prop));

  update(entity: Partial<S3Settings>): void {
    store.update(state => ({
      ...state,
      ...entity
    }));
  }
}
