import {catchError, from, map, Observable, of, switchMap, tap} from 'rxjs';
import {Injectable} from '@angular/core';
import {S3Repository, S3Settings} from './s3.repository.service';
import {LoadingStates} from '../../../../+share/loading-state/loadingStates';
import dayjs from 'dayjs';
import {PutObjectCommand, S3} from '@aws-sdk/client-s3';

@Injectable()
export class S3ClientService {
  private readonly apiVersion = '2006-03-01';
  private bucket = '/';

  constructor(private s3Repository: S3Repository) {
  }

  public validate(settings: S3Settings): Observable<boolean> {
    return this.createClient(settings)
      .pipe(
        tap(() => this.s3Repository.update({checkingState: LoadingStates.Loading, versioningState: null})),
        switchMap(x => {
          const promise = x.getBucketVersioning({Bucket: this.bucket});
          return from(promise).pipe(
            map(x => {
              this.s3Repository.update({
                checkingState: LoadingStates.Finished,
                versioningState: x.Status === 'Enabled',
                lastCheck: dayjs(),
              });

              return x.Status === 'Enabled';
            }),
            catchError(e => {
              this.s3Repository.update({
                checkingState: LoadingStates.Error,
                versioningState: null,
                lastCheck: dayjs(),
              });

              throw e;
            }),
          );
        })
      );
  }

  public read(settings: S3Settings, fileName: string): Observable<{ fileName: string, content: string }> {
    return this.createClient(settings)
      .pipe(
        tap(() => this.s3Repository.update({downloadingState: LoadingStates.Loading})),
        switchMap(x => {
          const promise = x.getObject({Bucket: this.bucket, Key: fileName});
          return from(promise).pipe(
            tap(() => this.s3Repository.update({downloadingState: LoadingStates.Finished})),
            map(() => ({fileName: fileName, content: ''})),
            catchError(e => {
              this.s3Repository.update({downloadingState: LoadingStates.Error});
              throw e;
            }),
          );
        })
      );
  }

  public write(settings: S3Settings, fileName: string, object: string): Observable<boolean> {
    return this.createClient(settings)
      .pipe(
        tap(() => this.s3Repository.update({uploadState: LoadingStates.Loading})),
        switchMap(x => {
          const promise = x.send(new PutObjectCommand( {
            Bucket: this.bucket,
            Key: fileName,
            ContentType: 'application/json',
            Body: object
          }));

          return from(promise).pipe(
            tap(() => this.s3Repository.update({uploadState: LoadingStates.Finished})),
            map(x => (!!x)),
            catchError(e => {
              this.s3Repository.update({uploadState: LoadingStates.Error});
              throw e;
            }),
          );
        })
      );
  }

  private createClient(x: S3Settings): Observable<S3> {

    const accessKeyId = x['accessKeyId'];
    const secretAccessKey = x['secretAccessKey'];
    const bucket = x['bucket'];
    const region = x['region'];

    if (!accessKeyId || !secretAccessKey || !bucket || !region) throw Error('config invalid');

    this.bucket = bucket;
    const s3 = new S3({
      apiVersion: this.apiVersion,
      credentials: {
        accessKeyId: accessKeyId,
        secretAccessKey: secretAccessKey,
      },
      region: region,
    });

    return of(s3);
  }
}
