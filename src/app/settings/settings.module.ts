import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettingsComponent} from './settings.component';
import {SettingsRoutingModule} from './settings-routing.module';
import {S3Module} from './components/s3/s3.module';
import {TRANSLOCO_SCOPE, TranslocoModule} from '@ngneat/transloco';

@NgModule({
  declarations: [
    SettingsComponent
  ],
  imports: [
    CommonModule,
    TranslocoModule,
    S3Module,
    SettingsRoutingModule,
  ],
  providers: [
    {provide: TRANSLOCO_SCOPE, useValue: 'settings'}
  ]
})
export class SettingsModule {
}
